from Queue import Queue
from argparse import ArgumentParser
from socket import SO_REUSEADDR, SOCK_STREAM, error, socket, SOL_SOCKET, AF_INET
from threading import Thread

parser = ArgumentParser(description='Multi-threaded TCP Client')
parser.add_argument('-r', '--requests', default=10, type=int, help='Total number of requests to send to server')
parser.add_argument('-w', '--workerThreads', default=5, type=int, help='Max number of worker threads to be created')
parser.add_argument('-i', '--ip', default='127.0.0.1', help='IP address to connect over')
parser.add_argument('-p', '--port', default=9000, type=int, help='Port over which to connect')
args = parser.parse_args()

class Client:
    def __init__(self, id, address, port, message):
        self.s = socket(AF_INET, SOCK_STREAM)
        self.id = id
        self.address = address
        self.port = port
        self.message = str(message)

    def run(self):
        try:
            self.s.settimeout(5)
            self.s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
            self.s.connect((self.address, self.port))
            self.s.send(self.message)
            data = self.s.recv(1024)
            print self.id, ":  received: ", data
            self.s.close()
        except error as e:
            print "\nERROR: Could not connect to ", self.address, " over port", self.port, "\n"
            raise e

q = Queue(maxsize=0)

def worker():
    message = "HELO"

    while True:
        item = q.get()

        new_client = Client(item, args.ip, args.port, message)
        new_client.run()
        q.task_done()

for item in range(args.requests):
    q.put(item)

for i in range(args.workerThreads):
    t = Thread(target=worker)
    t.daemon = True
    t.start()

q.join()
