------
# Topic: Lab 5
### Author: Sclifos Corina
------
## Objectives :
1. Get familiar with Sockets API of the chosen language;

2. Develop a transfer protocol of data(messages), using the TCP;

## Protocol description : 

1. Service - The service provided by the server part is to implement some basic commands which return text responses, also to store the messages sent by the and show the number of threads used and their order;

2. Transport/Execution path - We have the client which formulates the request and it is sent by the TCP to the server, next the server creates a thread which processes the request and returns the response;

3. Vocabulary(the Commands) :
  * '-r', '--requests';
  * '-w', '--workerThreads';
  * '-i', '--ip';
  * '-p', '--port';

4. Request format - Command [Message];
  
## Structure overview :

1. **Libraries** :

~~~py
from Queue import Queue
from argparse import ArgumentParser
from socket import SO_REUSEADDR, SOCK_STREAM, error, socket, SOL_SOCKET, AF_INET
from threading import Thread
~~~

* The "queue" module implements multi-producer, multi-consumer queues. It is especially useful in threaded programming when information must be exchanged safely between multiple threads. 
* The "argparse" module makes it easy to write user-friendly command-line interfaces. The program defines what arguments it requires, and argparse will figure out how to parse those out of sys.argv. 
* The Python interface is a straightforward transliteration of the Unix system call and library interface for sockets to Python’s object-oriented style: the socket() function returns a socket object whose methods implement the various socket system calls. *SO_REUSEADDR* flag tells the kernel to reuse a local socket in TIME_WAIT state, without waiting for its natural timeout to expire; *SOCK_STREAM* means that it is a TCP socket.

## ARGUMENT HANDLING

Initialize instance of an argument parser and optional arguments, with given default values if user gives no args
~~~py
parser = ArgumentParser(description='Multi-threaded TCP Client') 
parser.add_argument('-r', '--requests', default=10, type=int, help='Total number of requests to send to server')
parser.add_argument('-w', '--workerThreads', default=5, type=int, help='Max number of worker threads to be created')
parser.add_argument('-i', '--ip', default='127.0.0.1', help='IP address to connect over')
parser.add_argument('-p', '--port', default=9000, type=int, help='Port over which to connect')
~~~

## CLIENT CONSTRUCTOR

~~~py

lass Client:
    def __init__(self, id, address, port, message):
        self.s = socket(AF_INET, SOCK_STREAM)
        self.id = id
        self.address = address
        self.port = port
        self.message = str(message)

    def run(self):
        try:
            self.s.settimeout(5)  -->  Timeout if the no connection can be made in 5 seconds
            self.s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1) --> Allow socket address reuse
            self.s.connect((self.address, self.port)) --> Connect to the ip over the given port
            self.s.send(self.message) --> Send the defined request message
            data = self.s.recv(1024) --> Wait to receive data back from server
            print self.id, ":  received: ", data --> Notify that data has been received
            self.s.close()
        except error as e:
            print "\nERROR: Could not connect to ", self.address, " over port", self.port, "\n"
            raise e
~~~

## QUEUE WORKER FUNCTION
Function which generates a Client instance, getting the work item to be processed from the queue

~~~py
def worker():
    message = "HELO"

    while True:
        item = q.get()

        new_client = Client(item, args.ip, args.port, message)
        new_client.run()
        q.task_done()
~~~

## CLIENT WORKER THREADS 

* Populate the work queue with a list of numbers as long as the total number of requests wished to be sent.
* These queue items can be thought of as decrementing counters for the client thread workers.
* Create a number of threads, given by the maxWorkerThread variable, to initiate clients and begin sending requests.

~~~py
for item in range(args.requests):
    q.put(item)

for i in range(args.workerThreads):
    t = Thread(target=worker)
    t.daemon = True
    t.start()

~~~

## DEFINE GLOBAL VARIABLES


~~~py
counter = 0
response_message = "Now serving, number: "
thread_lock = Lock()

~~~

Create a server TCP socket and allow address re-use
~~~py
s = socket(AF_INET, SOCK_STREAM)
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
s.bind(('localhost', args.port))
~~~

Create a list in which threads will be stored in order to be joined.
~~~py
threads = []
~~~

## THREADED CLIENT HANDLER CONSTRUCTION 
Define the actions the thread will execute when called.

~~~py
class ClientHandler(Thread):
    def __init__(self, address, port, socket, response_message, lock):
        Thread.__init__(self)
        self.address = address
        self.port = port
        self.socket = socket
        self.response_message = response_message
        self.lock = lock
        
    def run(self):
        global counter
        self.socket.send(self.response_message + str(counter))
        with self.lock:
            counter += 1
        self.socket.close()
~~~



## MAIN-THREAD SERVER INSTANCE
Continuously listen for a client request and spawn a new thread to handle every request

~~~py
while 1:

    try:
        s.listen(1)
        sock, addr = s.accept()
        newThread = ClientHandler(addr[0], addr[1], sock, response_message, thread_lock)
        newThread.start()
        threads.append(newThread)
    except KeyboardInterrupt:
        print "\nExiting Server\n"
        break

~~~
When server ends, wait until remaining threads finish
~~~py
for item in threads:
    item.join()
~~~

## Result

![result](result.png)
