from argparse import ArgumentParser
from threading import Lock, Thread
from socket import SO_REUSEADDR, SOCK_STREAM, socket, SOL_SOCKET, AF_INET

parser = ArgumentParser(description='Multi-threaded TCP Server')
parser.add_argument('-p', '--port', default=9000, type=int, help='Port over which to connect')
args = parser.parse_args()

counter = 0
response_message = "Now serving, number: "
thread_lock = Lock()

s = socket(AF_INET, SOCK_STREAM)
s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
s.bind(('localhost', args.port))

threads = []

class ClientHandler(Thread):
    def __init__(self, address, port, socket, response_message, lock):
        Thread.__init__(self)
        self.address = address
        self.port = port
        self.socket = socket
        self.response_message = response_message
        self.lock = lock

    def run(self):
        global counter
        self.socket.send(self.response_message + str(counter))

        with self.lock:
            counter += 1
        self.socket.close()


while 1:

    try:
        s.listen(1)
        sock, addr = s.accept()
        newThread = ClientHandler(addr[0], addr[1], sock, response_message, thread_lock)
        newThread.start()
        threads.append(newThread)
    except KeyboardInterrupt:
        print "\nExiting Server\n"
        break

for item in threads:
    item.join()
