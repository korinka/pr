------
# Topic: Lab 6
### Author: Sclifos Corina
------
## Objectives :
1. Get familiar with Sockets API of the chosen language;

2. Develop a transfer protocol of data(messages), using the TCP and Multicast;

## Dealing with Multiple connections — Multicast: 

When dealing with multiple clients, maintaining several point-to-point connections can be cumbersome for applications due to the increases bandwidth and processing needs. This is where multicast messages come in. With multicast, messages are delivered to multiple endpoints simultaneously. This method achieves increased efficiency since delivery of messages to all recipients is delegated to the network infrastructure.

Multicast messages are sent using UDP, since TCP assumes a pair of communicating endpoints. The addresses for multicast messages, identified as multicast groups, are a subset of IPv4 addresses, usually between the range of 224.0.0.0 to 239.255.255.255. Network routers and switches treat addresses in this range as special since they are reserved for multicast, ensuring messages sent to the group are distributed to all clients that join the group.
  
## Send Multicast Messages :

To send messages we use an ordinary *sento()* method with a multicast group as the address. Moreover, we also need to specify a Time To Live(TTL) value which determines how far the messages should be broadcast from the sender. The default TTL of 1, will result in messages being sent only to hosts within the local network. We shall use *setsockopt()* with the* IP_MULTICAST_TTL* option to set the TTL, which should be packed into a single byte.
We shall also set a timeout value on the socket, to prevent it from waiting indefinitely for responses, since we have no idea how many responses we expect to get from the network.

~~~py
import socket
import struct

message = b'very important data'
multicast_group = ('224.10.10.10', 10000)

# Create the datagram socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Set a timeout so the socket does not block
# indefinitely when trying to receive data.
sock.settimeout(0.2)

# Set the time-to-live for messages to 1 so they do not
# go past the local network segment.
ttl = struct.pack('b', 1)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

try:

    # Send data to the multicast group
    print('sending {!r}'.format(message))
    sent = sock.sendto(message, multicast_group)

    # Look for responses from all recipients
    while True:
        print('waiting to receive')
        try:
            data, server = sock.recvfrom(16)
        except socket.timeout:
            print('timed out, no more responses')
            break
        else:
            print('received {!r} from {}'.format(
                data, server))

finally:
    print('closing socket')
    sock.close()
~~~
* We create a socket of type *socket.SOCK_DGRAM* , compose or message as a byte string and bind our socket to the multicast group address* (‘224.10.10.10’, 10000) *.
* We then set a timeout of *0.2* using *sock.settimeout(0.2)* .
* Using the *struct* module, we pack the number 1 into a byte and assign the byte to* ttl *.
* Using *setsockopt()* we set the *IP_MULTICAST_TTL* option of the socket to the *ttl* we just created above and send the message using *sendto() *.
* We then wait for responses from other hosts on the network, as long as the wait hasn’t timed out and print out the responses.

## Receiving Multicast Messages

To receive messages, after creating our ordinary socket and binding it to a port, we would need to add it to the multicast group. This can be done by using the *setsockopts()* to set the *IP_ADD_MEMBERSHIP* option, which should be packed into an 8-byte representation of the multicast group, and the network interface on which the server should listen for connections. We shall use *socket.inet_aton()* to convert the multicast group IPv4 address from dotted-quad string format *(‘224.10.10.10’)* to 32-bit packed binary format.

~~~py
import socket
import struct
import sys

multicast_group = '224.10.10.10'
server_address = ('', 10000)

# Create the socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind to the server address
sock.bind(server_address)

# Tell the operating system to add the socket to
# the multicast group on all interfaces.
group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(
    socket.IPPROTO_IP,
    socket.IP_ADD_MEMBERSHIP,
    mreq)

# Receive/respond loop
while True:
    print('\nwaiting to receive message')
    data, address = sock.recvfrom(1024)

    print('received {} bytes from {}'.format(
        len(data), address))
    print(data)

    print('sending acknowledgement to', address)
    sock.sendto(b'ack', address)
~~~
* To add the socket to the multicast group, we use *struct* to pack the group address given by *socket.inet_aton(multicast_group)* and the network interface given by *socket.INADDR_ANY * into an 8-byte representation, which we then set to the socket’s *IP_ADD_MEMBERSHIP* .
* As the socket receives messages, we use *recvfrom(1024)* to unpack the response into *data* and* address *, and send out an acknowlegement to the *address* using *sendto()* .
* 
## Result


~~~py
corina@corincic:~/Desktop/UDP$ python3 server.py
sending b'very important data'
waiting to receive
received b'ack' from ('192.168.100.2', 10000)
waiting to receive
received b'ack' from ('192.168.100.13', 10000)
waiting to receive
timed out, no more responses
closing socket
~~~

~~~py
corina@corincic:~/Desktop/UDP$ python3 client.py
waiting to receive message
received 19 bytes from ('192.168.100.2', 48290)
b'very important data'
sending acknowledgement to ('192.168.100.2', 48290)
waiting to receive message
~~~

