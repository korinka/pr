# Sclifos Corina --> FAF-161
# Lab 3 ----> HTTP Protocol


## How it works

Python HTTP module defines the classes which provide the client-side of the HTTP and HTTPS protocols. In most of the programs, the HTTP module is not directly used and is clubbed with the `urllib` module to handle URL connections and interaction with HTTP requests. For this wark I used `http.client` module it is pretty similar with `urllib`. 

Philosophy:
* Beautiful is better than ugly
* Explicit is better than implicit
* Simple is better than complex
* Complex is better than complicate

In this lab on python HTTP module, I will try attempting making connections and making HTTP requests like GET, POST and PUT.

### What is an HTTP request
![](1.png)


## Making HTTP Connections
I started with the simplest thing HTTP module can do. I can easily make HTTP connections using this module. Here is a sample program:
~~~python
import http.client

connection = http.client.HTTPConnection('http://www.httpbin.org/')
print(connection)
~~~

## Python HTTP GET

GET is used to request data from a specified resource.

~~~python
import http.client

connection = http.client.HTTPSConnection('www.httpbin.org')
connection.request("GET", "/")
response = connection.getresponse()
print("Status: {} and reason: {}".format(response.status, response.reason))

connection.close
~~~

Output:
```
Status: 200 and reason: OK

```
## Python HTTP POST

POST is used to send data to a server to create/update a resource.

~~~python
import http.client
import json

conn = http.client.HTTPSConnection('www.httpbin.org')

headers = {'Content-type': 'application/json'}

foo = {'text': 'Hello HTTP #1 **cool**, and #1!'}
json_data = json.dumps(foo)

conn.request('POST', '/post', json_data, headers)

response = conn.getresponse()
print(response.read().decode())
~~~

Output:
```
{
  "args": {}, 
  "data": "{\"text\": \"Hello HTTP #1 **cool**, and #1!\"}", 
  "files": {}, 
  "form": {}, 
  "headers": {
    "Accept-Encoding": "identity", 
    "Content-Length": "43", 
    "Content-Type": "application/json", 
    "Host": "www.httpbin.org"
  }, 
  "json": {
    "text": "Hello HTTP #1 **cool**, and #1!"
  }, 
  "origin": "89.41.94.125, 89.41.94.125", 
  "url": "https://www.httpbin.org/post"
}
```
## Python HTTP PUT Request


PUT is used to send data to a server to create/update a resource.


~~~python
import http.client
import json

conn = http.client.HTTPSConnection('www.httpbin.org')

headers = {'Content-type': 'application/json'}

foo = {'text': 'Hello HTTP #1 **cool**, and #1!'}
json_data = json.dumps(foo)


conn.request("PUT", "/put", json_data)
response = conn.getresponse()
print(response.status, response.reason)
~~~

Output:
```
200 OK

```
#### Note: What about the other HTTP request types: DELETE, HEAD and OPTIONS...? These are all implemented like first free. You just need to change GET to HEAD.

##  Link: https://gitlab.com/korinka/pr/tree/master/lab3
