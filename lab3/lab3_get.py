import http.client

connection = http.client.HTTPSConnection('www.httpbin.org')
connection.request("GET", "/")
response = connection.getresponse()
print("Status: {} and reason: {}".format(response.status, response.reason))

connection.close