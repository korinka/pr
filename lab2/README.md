# Sclifos Corina --> FAF-161
# Lab 2 ----> Multithreading


## How it works

To create threading in Python. First thing you need to do is to import Thread using the following code:

~~~python
from threading import Event, Thread, Lock
~~~

For implementing this laboratory work I used  `Event` object provided by python. It has 2 main functions: `event.wait()` and `event.set()`.
An event manages a flag that can be set to true with the set() method . The wait() method blocks until the flag is true.
Lock() - The class implementing primitive lock objects. Once a thread has acquired a lock, subsequent attempts to acquire it block, until it is released; any thread may release it.

To create a thread in Python you'll want to make your class work as a thread. For this, you should subclass your class from the Thread class:
I created a class `MyTask` with the following constructor:
~~~python
def __init__(self, action, events_to_wait, event_to_set):
~~~

Now, our MyTaskclass is a child class of the Thread class. We then define a run method in our class. This function will be executed when we call the start method of any object in our MyTask class.

~~~python
def run(self):
    # Wait for all required events
    for event_to_end in self.events_to_wait:
        event_to_end.wait()
    # Run action
    self.action()
    # Set that it's ready
    self.event_to_set.set()
~~~

Where:
  * action - a lambda
  * events_to_wait - the events to wait before executing the action
  * event_to_set - the event to set after action is done

The tasks are created in the following way:
~~~python
def init_tasks():
  tasks = [None] * 7

  tasks[0] = MyTask(
      lambda_print('task1'),
      [task_events[1], task_events[2]],
      task_events[0])

  tasks[1] = MyTask(
      lambda_print('task2'),
      [task_events[4], task_events[5]],
      task_events[1])

  tasks[2] = MyTask(
      lambda_print('task3'),
      [task_events[3], task_events[6]],
      task_events[2])

  tasks[3] = MyTask(
      lambda_print('task4'),
      [],
      task_events[3])

  tasks[4] = MyTask(
      lambda_print('task5'),
      [],
      task_events[4])

  tasks[5] = MyTask(
      lambda_print('task6'),
      [],
      task_events[5])

  tasks[6] = MyTask(
      lambda_print('task7'),
      [],
      task_events[6])

  return tasks
~~~

`task_events` is an array with `Event` objects, each event corresponding to a task.

### Lock

Because printing can't be done in the same time from multiple threads, it has to be locked. 
In this way we don't need acquire() and release(), because it is made automatic.

~~~python
def my_print(some_str):
    with print_lock:
        print(some_str)
~~~