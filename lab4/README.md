# Network Programming Laboratory Work Nr.4

## Prerequisites
  * SMTP - Simple Mail Transfer Protocol
  * POP3 - Post Office Protocol 3

## Objectives
  * Understand the SMTP protocol and the basic methods
  * Initialization the properties for SMTP Protocol
  * Using the real mail account/fake server for testing the mail sending functionalities
  * Basic concepts of SMTP/POP3 - Protocols

## Task
Read/Send an email using the SMTP and POP3 protocols

## How it works
![](./img/pop.png)

SMTP and POP3 are the protocols used most in the Internet because they provide the necessary services to handle electronic mails (emails).

The SMTP protocol is responsible for putting the email in mailboxes, and when it comes to removing the messages from there, it is necessary to use the POP3 protocol. The Post Office Protocol (POP) is used by mail readers that work on network clients and are connected to designated mail servers to send and receive mail. The purpose of this protocol is to allow remote access to a mailbox that is hosted by an external server.

For your information, SMTP is also used to send the messages across the Internet. Anyone who writes a POP client can communicate with a POP server because this protocol abstracts the details of the email to a system-independent level. This protocol was designed so that users couldaccess their mail from machines that weren't configured for receiving mail. Also, all systems on the Internet mail system agree to use SMTP to handle mail. Storage of mail can vary on different systems, although this is not an OS issue, but an application issue.

### Mail sending

![](./img/60229412_449041575843726_389416541470326784_n.jpg)

### Steps I use to send mail with Attachments from Gmail account:
* For adding an attachment, you need to import:
    ~~~py
    import smtplib
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    from email.mime.base import MIMEBase
    from email import encoders
    ~~~
    These are some libraries which will make our work simple. These are the native libraries and you dont need to import any external library for this.
* Firstly, create an instance of MIMEMultipart, namely “msg” to begin with.
* Mention the sender’s email id, receiver’s email id and the subject in the “From”, “To” and “Subject” key of the created instance “msg”.
* In a string, write the body of the message you want to send, namely body. Now, attach the body with the instance msg using attach function.
* Open the file you wish to attach in the “rb” mode. Then create an instance of MIMEBase with two parameters. First one is ‘_maintype’ amd the other one is ‘_subtype’. This is the base class for all the MIME-specific sub-classes of Message. Note that ‘_maintype’ is the Content-Type major type (e.g. text or image), and ‘_subtype’ is the Content-Type minor type (e.g. plain or gif or other media).
* set_payload is used to change the payload the encoded form. Encode it in encode_base64. And finally attach the file with the MIMEMultipart created instance msg.


### Mail reading

Sample output:

~~~
Subject: Testig read.py
From: Corina Sclifos <korinu6a@gmail.com>
To: korinu6a@gmail.com
Body: Hi there, sending this email from Python!
Headers: ----------------------------------------------------------
Delivered-To: korinu6a@gmail.com
Received: by 2002:a67:7f4b:0:0:0:0:0 with SMTP id a72csp3337862vsd;
        Mon, 8 Apr 2019 03:03:11 -0700 (PDT)
X-Received: by 2002:a9d:12a2:: with SMTP id g31mr19414159otg.174.1554717791871;
        Mon, 08 Apr 2019 03:03:11 -0700 (PDT)
ARC-Seal: i=1; a=rsa-sha256; t=1554717791; cv=none;
        d=google.com; s=arc-20160816;
        b=KlW80UBjzzGPbtT9Yne00LKrMLStCSGzqfo3+iiO9lz/tAgE+9PkNJ1o6eHYpanTg1
         gA2d/0ysL8hKhc1KNm+fcw/3vAKs4AM/Q3ajesYX1Dp8gUkhpqDg50eqZL2dSfr/w/ql
         voqEm0p1o5klPKuuYm7cgzbpSy0LmRg+1ZkEphSql4zPOFP0cDeV0ePECfTl1X3GMj5D
         BU4eNOXKCEOOGhbfHZxnbh/3sZKz63RwMnExqiLvWNdcTKBnsVb/gYi4Va0bRONKd7zL
         EnNYQSeT1Tx+hp4GysRVuTnB3Hxa5FsgCJFZ0gfVgJL9YfxsM1x7sYEMfEQk0FczQXIz
         VHZA==
ARC-Message-Signature: i=1; a=rsa-sha256; c=relaxed/relaxed; d=google.com; s=arc-20160816;
        h=to:subject:message-id:date:from:mime-version:dkim-signature;
        bh=lq2XVBGGNtO0sf/U4duSGJQPmbeXAcTZy95LNBPwd7Y=;
        b=xUHR62ETdobtzi+IVIc0ObXBDzPcGPgqCua28mbqsHGdYISG3wY+6aQxz516qdCAsR
         53OVYIK+Z6CCRSiuzjJZp0l/pckSdGetygl6dbkJhRxpgpElLgV29DH6u6CzOhMH/m90
         CRHK3Gmw4VeBgoA4ZGklDmGa79NFLtnxWrgWW4aXyz8uBsOmd+CzfIeVF/3heX6390vQ
         H7TZfDvBMay7RLl2HHSV/1O+jfVpaacOrMENregosUqM/qJmJ0zVA09RiVreSOy/xNBu
         GnnQ+4hzBM4TfTDyNXYrcNucF4x/BdmRF223MCHVj+hVd7jq+nnDgghmD9NYhqdLsA0P
         bouw==
ARC-Authentication-Results: i=1; mx.google.com;
       dkim=pass header.i=@gmail.com header.s=20161025 header.b=LPCongZk;
       spf=pass (google.com: domain of korinu6a@gmail.com designates 209.85.220.41 as permitted sender) smtp.mailfrom=korinu6a@gmail.com;
       dmarc=pass (p=NONE sp=QUARANTINE dis=NONE) header.from=gmail.com
Return-Path: <korinu6a@gmail.com>
Received: from mail-sor-f41.google.com (mail-sor-f41.google.com. [209.85.220.41])
        by mx.google.com with SMTPS id p125sor11265376oia.109.2019.04.08.03.03.11
        for <korinu6a@gmail.com>
        (Google Transport Security);
        Mon, 08 Apr 2019 03:03:11 -0700 (PDT)
Received-SPF: pass (google.com: domain of korinu6a@gmail.com designates 209.85.220.41 as permitted sender) client-ip=209.85.220.41;
Authentication-Results: mx.google.com;
       dkim=pass header.i=@gmail.com header.s=20161025 header.b=LPCongZk;
       spf=pass (google.com: domain of korinu6a@gmail.com designates 209.85.220.41 as permitted sender) smtp.mailfrom=korinu6a@gmail.com;
       dmarc=pass (p=NONE sp=QUARANTINE dis=NONE) header.from=gmail.com
DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=gmail.com; s=20161025;
        h=mime-version:from:date:message-id:subject:to;
        bh=lq2XVBGGNtO0sf/U4duSGJQPmbeXAcTZy95LNBPwd7Y=;
        b=LPCongZkT+8Cw8gQxhovYcYVE+xBFBs9bfz3p+ExVqrAZhMc0dH+/lXsyOEBOBepBH
         aYqZ0oxEqpKXq2X0Pylhk+csueHU1ZAWcBQvtMiet5d0++gaZwPpkI1Up57ml7wKv1iN
         8xu900VOd621L91vVIs7BCQatvLSJSCV2yRJm/cF5PIxJez2zX0DCZ20xXjB9yU//hBs
         pE2YDnOIQE/39J/5Cgok82Q/EzQCTGN5WoPxqJQyBC9i3YinSR4MmIAuHPVTkVY5PLiH
         DsPZ+Oocd5zr80/+tBNDLlBwVb1gUCklyE2w8UYwz5a5oekuqtKvRB2rthnuF2TBEgBh
         v4Wg==
X-Google-DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=1e100.net; s=20161025;
        h=x-gm-message-state:mime-version:from:date:message-id:subject:to;
        bh=lq2XVBGGNtO0sf/U4duSGJQPmbeXAcTZy95LNBPwd7Y=;
        b=l/klfkx8NGGo/pGCXO0RD2xsSm1BNPs0C+QPIe+K4QwYpbY5D74C3/IxopnUUjRJN8
         Ggg0At6o05e62BGTPc5JEhvm6CJoDbbl3opQU2nhC9R54U8GOru7zC6dB/knAuD5w63g
         Q4AtppkvmoSeqQIm2IAYygv8gxEF/7G8MH3f5E5ECEXrb+PymMDNEE0AeVC74N3RI0lf
         pi+aknAnLb2XV4jt3ezVywPX2ykf099plheGN+MTzfvMb7ZRnGxSxcK65kJTa4JBeE8F
         i0h6gqIA6XMwVcx4DHF+RaBs305QBJUGswjtWTmc+9I8bGR387PhGVsxlFjHSXJ5wkpu
         gkUg==
X-Gm-Message-State: APjAAAXQJ80Z6MwhLVhXyOyDGVKfIl+v9thG4T2+fSjO5jCJ45cj1xCr
	MF8oS7NJMCQBgCSKdXU5yE3XNbpxM8g5DhRkdU/Bhfb0
X-Google-Smtp-Source: APXvYqy52+muuR14ZItutgzTwyt4gkSJGRIIN6y6VuXQpg0rNYf414rW+5mQOitcoeRlZtVBmlc/AtS2ArbhsT7Sbt8=
X-Received: by 2002:aca:32c2:: with SMTP id y185mr16534018oiy.177.1554717791354;
 Mon, 08 Apr 2019 03:03:11 -0700 (PDT)
MIME-Version: 1.0
From: Corina Sclifos <korinu6a@gmail.com>
Date: Mon, 8 Apr 2019 13:03:00 +0300
Message-ID: <CA+w+g2eBv3hDL4Q8842f+aLsocxnZfCb8RZZ=RRw=eJaeBs36Q@mail.gmail.com>
Subject: Testing read.py
To: korinu6a@gmail.com
Content-Type: multipart/alternative; boundary="000000000000ae062e058601f1c5"
~~~


The poplib module included with Python provides simple access to POP3 mail servers that allow you to connect and quickly retrieve messages using your Python scripts .

### Gmail (POP) Server Settings
  * Pop3Server : pop.gmail.com
  * Requires SSL: Yes
  * Port: 995

For readind emails, you need to import:
~~~py
import sys
import poplib
import email
~~~
 These are some libraries which will make our work simple. These are the native libraries and you dont need to import any external library for this.

## Conclusion
No particular tricks or hacks are used in this laboratory work lust  piece of codes, simple examples of POP3 and SMTP usage.
Thanks Indian guys for help.